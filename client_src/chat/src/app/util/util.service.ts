import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import * as globalVar from '../util/global';
import { UtilComponent } from '../util/util.component';
import { UserDetail, ConvDetail } from './util.model';

@Injectable()
export class UtilService {

      hostname: string = '';
      private chatSubject: Subject<string> = new Subject<string>();
      public headers: Headers;
      constructor(private http: Http, private uC: UtilComponent) {
            this.headers = new Headers();
            this.headers.append('Access-Control-Allow-Headers', 'Content-Type');
            this.headers.append('Access-Control-Allow-Methods', 'GET, POST');
            this.headers.append('Access-Control-Allow-Origin', '*');
            this.headers.append('Content-Type', 'application/json');
      }


      private handleError(error: any) {
            let errMsg = (error.message) ? error.message :
                  error.status ? `${error.status} - ${error.statusText}` : 'Server error';
            console.error(errMsg);
            return Observable.throw(errMsg);
      }

      private extractData(res: Response) {
            let body = res.json()
            return body || {};
      }
      private extractedData(res: Response) {
            let body = res.json()
            return JSON.stringify(body || {});
      }
      public getUserDetail(email: string): Observable<UserDetail> {
            let body = new URLSearchParams();
            let _userDetail=new UserDetail();
            _userDetail.email=email;
            return this.http
                  .post('http://localhost:8080/chat/addUser',_userDetail)
                  .map<UserDetail>((res) => this.extractData(res))
                  .catch<UserDetail>((err) => this.handleError(err));
      }
      public getUserList(): Observable<UserDetail> {
            console.log('@@@@@@@@@@')
            return this.http
                  .get('http://localhost:8080/chat/getAllUser')
                  .map<UserDetail>((res) => this.extractData(res))
                  .catch<UserDetail>((err) => this.handleError(err));
      }
      public getConvDetail(convDetail:ConvDetail): Observable<ConvDetail> {
            let body = new URLSearchParams();
            return this.http
                  .post('http://localhost:8080/chat/convId', convDetail)
                  .map<ConvDetail>((res) => this.extractData(res))
                  .catch<ConvDetail>((err) => this.handleError(err));
      }
}

