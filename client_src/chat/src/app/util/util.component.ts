
import { Component } from './../../../../node_modules/@angular/core';
import { ReplaySubject } from 'rxjs';


@Component({
    selector: 'stringutil',
    template: ``
    
})

export class UtilComponent {
    private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
    public isAuthenticated = this.isAuthenticatedSubject.asObservable();
    public scrollToBottomOfDiv(divId: string, timer: number = 0) {
      setTimeout(() => {
          var objDiv = document.getElementById(divId);
          if (objDiv != undefined && objDiv != null) {
              objDiv.scrollTop = objDiv.scrollHeight+1000;
          }
      }, timer)
  }
}