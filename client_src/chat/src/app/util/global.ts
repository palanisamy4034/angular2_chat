export var userName: any = null;
export var convId: number = null;
export var senderId: number = null;
export var reciverId: number = null;
export var senderemail: string = null;
export function setUserName(userName: any) {
      this.userName = userName;
}
export function getUserName() {
      return this.userName;
}
export function setConvId(convId: any) {
      this.convId = convId;
}
export function getConvId() {
      return this.convId;
}
export function setSenderId(senderId: any) {
      this.senderId = senderId;
}
export function getSenderId() {
      return this.senderId;
}
export function setReciverId(reciverId: any) {
      this.reciverId = reciverId;
}
export function getReciverId() {
      return this.reciverId;
}
export function setSenderEmail(senderemail: any) {
      this.senderemail = senderemail;
}
export function getSenderEmail() {
      return this.senderemail;
}