import { ModuleWithProviders } from '../../../node_modules/@angular/core';
import { Routes, RouterModule, RouterOutlet, RouterLink, RouterLinkWithHref, RouterLinkActive } from '../../../node_modules/@angular/router';
import { AppComponent } from './app.component';
import { RegisterComponent } from './user/register/register.component';
import { LoginComponent } from './user/login/login.component';
import { ChatHomeComponent } from './chatHome/chatHome.component';


export const appRoutes: Routes = [
    {
        path: '',
        redirectTo: 'chatHome',
        pathMatch: 'full'
    },
    {
        path: 'chatHome',
        component: ChatHomeComponent,
    }, {
        path: 'register',
        component: RegisterComponent,
    }, {
        path: 'login',
        component: LoginComponent,
    },
];
