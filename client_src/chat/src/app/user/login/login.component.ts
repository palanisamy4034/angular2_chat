
import { ViewChild, Component, Input, OnInit, trigger, state, style, animate, transition, ViewContainerRef } from '../../../../../node_modules/@angular/core';
import { Router, ActivatedRoute, Params } from '../../../../../node_modules/@angular/router';
import { UserLogin } from './login.model';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { connect } from 'net';
import { UserService } from './../user.service';
import * as globalVar from '../../util/global';
import { UtilComponent } from '../../util/util.component';
//import 'sockjs-client';
@Component({
	selector: 'login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css'],
	providers: []
})

export class LoginComponent{
	loginForm: FormGroup;
	private userLogin = UserLogin;
	constructor(private _fb: FormBuilder, private userServ: UserService,private rtr:Router) { }
	ngOnInit(): void {
		this.loginForm = this._fb.group({
			password: ['', <any>Validators.required],
			email: ['', <any>Validators.required],
		});
	}

	regSave(model: any, isValid: boolean) {
		if (isValid) {
			let _userReg = new UserLogin();
			_userReg.password = model.password;
			_userReg.email = model.email;
			this.userServ.submitLoginForm(_userReg).subscribe(data => {				
				if (data.status) {
					sessionStorage.setItem('uid',data.id	);
					sessionStorage.setItem('email',model.email);
					globalVar.setSenderId(data.id);
					globalVar.setSenderEmail(model.email)
					this.rtr.navigate(['/chatHome']);
				} else {

				}
			}, (error: any) => {

			});
		}

	}

}


