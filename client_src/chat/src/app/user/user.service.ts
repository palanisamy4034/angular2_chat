import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import * as globalVar from '../util/global';
import {UtilComponent} from '../util/util.component';
import { UserReg } from './register/register.model';
import { UserLogin } from './login/login.model';



declare var SockJS: any;
declare var Stomp: any
let _stompClient: any;
@Injectable()
export class UserService {

    hostname: string = '';
    private chatSubject: Subject<string> = new Subject<string>();
    public headers: Headers;
    constructor(private http: Http,private uC:UtilComponent) {
        this.headers = new Headers();
        this.headers.append('Access-Control-Allow-Headers', 'Content-Type');
        this.headers.append('Access-Control-Allow-Methods', 'GET, POST');
        this.headers.append('Access-Control-Allow-Origin', '*');
        this.headers.append('Content-Type', 'application/json');
    }


    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    private extractData(res: Response) {
        let body = res.json()   
        return body || {};
    }
    private extractedData(res: Response) {
        let body = res.json()
        return JSON.stringify(body || {});
    }

    public submitRegForm(_userReg:UserReg): Observable<any> {
        return this.http
            .post('http://localhost:8080/chat/addUser',  _userReg)
            .map<any>((res) => this.extractData(res))
            .catch<any>((err) => this.handleError(err));
    }
    public submitLoginForm(_userLogin:UserLogin): Observable<any> {
        let _body = new URLSearchParams();
        console.log(_userLogin)
        return this.http
            .post('http://localhost:8080/chat/userLogin',  _userLogin)
            .map<any>((res) => this.extractData(res))
            .catch<any>((err) => this.handleError(err));
    }

}
