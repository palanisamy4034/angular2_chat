
import { ViewChild, Component, Input, OnInit, trigger, state, style, animate, transition, ViewContainerRef } from '../../../../../node_modules/@angular/core';
import { Router, ActivatedRoute, Params } from '../../../../../node_modules/@angular/router';
import { UserReg } from './register.model';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { connect } from 'net';
import { UserService } from './../user.service';
import * as globalVar from '../../util/global';
import { UtilComponent } from '../../util/util.component';
//import 'sockjs-client';
@Component({
	selector: 'register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css'],
	providers: [],
	animations: [
		trigger('dialog', [
		  transition('void => *', [
		    style({ transform: 'scale3d(.3, .3, .3)' }),
		    animate(100)
		  ]),
		  transition('* => void', [
		    animate(100, style({ transform: 'scale3d(.0, .0, .0)' }))
		  ])
		])
	    ]
	  
})

export class RegisterComponent {
	regForm: FormGroup;
	private userReg = UserReg;
	constructor(private _fb: FormBuilder, private userServ: UserService,private rtr:Router) { }
	ngOnInit(): void {
		this.regForm = this._fb.group({
			username: ['', <any>Validators.required],
			email: ['', <any>Validators.required],
			password: ['', <any>Validators.required],
			mobile: ['', <any>Validators.required]
		});
	}

	regSave(model: any, isValid: boolean) {
		if (isValid) {
			let _userReg = new UserReg();
			_userReg.userName = model.username;
			_userReg.password = model.password;
			_userReg.email = model.email;
			_userReg.mobile = model.mobile;
			console.log(_userReg,'**********');
			this.userServ.submitRegForm(_userReg).subscribe(data => {
				globalVar.setConvId(data.id)
				sessionStorage.setItem('uid',data.id	);
				sessionStorage.setItem('email',model.email);
				globalVar.setSenderId(data.id);
				globalVar.setSenderEmail(model.email)
				this.rtr.navigate(['/chatHome']);
			});
		}

	}

}


