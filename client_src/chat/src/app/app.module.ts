import { ChatHomeComponent } from './chatHome/chatHome.component';
/**core angular components */
import { NgModule } from '../../../node_modules/@angular/core';
import { Component, Input, OnInit, trigger, state, style, animate, transition } from '../../../node_modules/@angular/core';
import { FormsModule, ReactiveFormsModule } from '../../../node_modules/@angular/forms'
import { BrowserModule } from '../../../node_modules/@angular/platform-browser';
import { HttpModule } from '../../../node_modules/@angular/http';
import { appRoutes } from './app.routing'
import { RouterModule, Routes } from '../../../node_modules/@angular/router'

/*app component */
import { AppComponent } from './app.component';
/**third party components */
import { ModalModule } from '../../../node_modules/ng2-bootstrap/ng2-bootstrap';
import { TabsModule } from '../../../node_modules/ng2-bootstrap/ng2-bootstrap';
import { Angulartics2Module, Angulartics2GoogleAnalytics, Angulartics2 } from '../../../node_modules/angulartics2';
import { RatingModule } from '../../../node_modules/ng2-bootstrap/ng2-bootstrap';
import { CustHomeService } from './chatHome/chatHome.service';
import { UtilComponent } from './util/util.component'
import { RegisterComponent } from './user/register/register.component';
import { LoginComponent } from './user/login/login.component';
import { UserService } from './user/user.service';
import { UtilService } from './util/util.service';
import { CategoryPipe } from './util/util.component.pipe';

@NgModule({
    imports:
    [
        BrowserModule,
        ReactiveFormsModule,
        HttpModule,
        ModalModule,
        TabsModule,
        Angulartics2Module.forRoot([Angulartics2GoogleAnalytics]),
        Angulartics2Module.forChild(),
        RatingModule,
        RouterModule.forRoot(appRoutes, { useHash: false }),
    ],
    declarations: [
        AppComponent, //app
        ChatHomeComponent, LoginComponent, RegisterComponent,CategoryPipe,
    ],
    providers: [CustHomeService, UtilComponent,UserService,UtilService],
    bootstrap: [AppComponent]
})
export class AppModule { }