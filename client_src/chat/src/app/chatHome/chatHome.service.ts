import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { messageTranscript, ChatMessage } from './chatHome.model';
import * as globalVar from '../util/global';
import {UtilComponent} from '../util/util.component';
import { ChatHomeComponent } from './chatHome.component';



declare var SockJS: any;
declare var Stomp: any
let _stompClient: any;
@Injectable()
export class CustHomeService {
    newMessageSubject: Subject<messageTranscript> = new Subject<messageTranscript>();;
    connectSubject: Subject<string> = new Subject<string>();

    private disconnectSubject: Subject<string> = new Subject<string>();
    private newMessage: messageTranscript = new messageTranscript();


    hostname: string = '';
    private chatSubject: Subject<string> = new Subject<string>();
    public headers: Headers;
    constructor(private http: Http,private uC:UtilComponent) {
        this.headers = new Headers();
        this.headers.append('Access-Control-Allow-Headers', 'Content-Type');
        this.headers.append('Access-Control-Allow-Methods', 'GET, POST');
        this.headers.append('Access-Control-Allow-Origin', '*');
        this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
    }

    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    private extractData(res: Response) {
        let body = res.json()
        return body || {};
    }
    private extractedData(res: Response) {
        let body = res.json()
        return JSON.stringify(body || {});
    }

    public connectWebSocket() {
        var that = this;
        var self = this;
        var socket = new SockJS('//localhost:8080/ws');
        _stompClient = Stomp.over(socket);
        //console.log(stompClient)
        _stompClient.connect({}, function (frame: any) {
            console.log('Connected: ' + frame);
            _stompClient.subscribe('/channel/public.'+globalVar.getSenderId(), function (greeting: any) {
                console.log(greeting.body);
                self.newMessageSubject.next(JSON.parse(greeting.body));
            });
        }, function (message: any) {
            console.log('Websocket disconnected.... Retrying in 3 seconds');
            self.disconnectSubject.next(_stompClient);
            setTimeout(() => {
                self.connectWebSocket();
            }, 3000);
        });
    }
    public sendNewMessage(_message: ChatMessage) {
       
        if (_stompClient) {
           
            console.log(globalVar.getReciverId,'@@@@@@@@@@@')
            _stompClient.send("/app/chat.sendMessage."+globalVar.getReciverId(), {}, JSON.stringify(_message));
        }
    }


    public onWebsocketConnect(): Observable<string> {
        return this.connectSubject.asObservable();
    }
    public onWebsocketDisconnect(): Observable<string> {
        return this.disconnectSubject.asObservable();
    }
    public onNewMessage(): Observable<messageTranscript> {
        return this.newMessageSubject.asObservable();
    }

}
