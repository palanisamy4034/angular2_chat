
import { ViewChild, Component, Input, OnInit, trigger, state, style, animate, transition, ViewContainerRef } from '../../../../../node_modules/@angular/core';
import { Router, ActivatedRoute, Params } from '../../../../../node_modules/@angular/router';
import { ChatMessage } from './../chatHome.model';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { connect } from 'net';
import { CustHomeService } from './../chatHome.service';
import * as globalVar from './../../util/global';
import { UtilComponent } from './../../util/util.component';
import { UtilService } from './../../util/util.service';
import { UserDetail, ConvDetail } from './../../util/util.model';
import {CategoryPipe} from './../../util/util.component.pipe'
//import 'sockjs-client';
@Component({
	selector: 'chatusers',
	templateUrl: './chatUsers.component.html',
	styleUrls: ['./chatUsers.component.css'],
})

export class ChatUsersComponent implements OnInit {
	uId: any;
	public myForm: FormGroup;
	public userDetail: UserDetail[] = [];
	public userData: any;
	constructor(private cHS: CustHomeService, private uC: UtilComponent, private utilServ: UtilService, private rtr: Router) { }
	ngOnInit(): void {
	let _uid = sessionStorage.getItem('uid')
	globalVar.getSenderEmail() == '' ? globalVar.setSenderEmail(sessionStorage.getItem('email')) : '';
	_uid != '' ? globalVar.setSenderId(_uid) : this.rtr.navigate(['/login']);
	this.utilServ.getUserDetail(globalVar.getSenderEmail()).subscribe(data => {
		this.cHS.connectWebSocket();
	});
	this.utilServ.getUserList().subscribe(data => {
		this.userDetail.push(data);
		this.userData = this.userDetail[0];
		this.uId = globalVar.getSenderId();
	});
}
}