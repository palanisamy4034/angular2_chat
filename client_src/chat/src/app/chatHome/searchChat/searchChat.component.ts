
import { ViewChild, Component, Input, OnInit, trigger, state, style, animate, transition, ViewContainerRef } from '../../../../../node_modules/@angular/core';
import { Router, ActivatedRoute, Params } from '../../../../../node_modules/@angular/router';
import { chatMessage } from '.././chatHome.model';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { connect } from 'net';
import { CustHomeService } from '.././chatHome.service';
import * as globalVar from '../../util/global';
import { UtilComponent } from '../../util/util.component';
import { UtilService } from '../../util/util.service';
import { UserDetail } from '.././../util/util.model';
//import 'sockjs-client';
@Component({
      selector: 'chatHome',
      providers: []
})

export class ChatHomeComponent implements OnInit {
      public myForm: FormGroup;
      public submitted: boolean;
      public messageTranscript: chatMessage[] = [];
      public userDetail: UserDetail[] = [];
      public msg: any = [];
      public uId: any;
      public userData: any;

      constructor(private _fb: FormBuilder, private cHS: CustHomeService, private uC: UtilComponent, private utilServ: UtilService) { }
      ngOnInit(): void {
            
            this.utilServ.getUserList().subscribe(data => {
                  this.userDetail.push(data);
                  this.userData = this.userDetail[0];
            });

            this.myForm = this._fb.group({
                  message: ['', <any>Validators.required]
            });
            this.cHS.onNewMessage().subscribe(data => {
                  this.uId = globalVar.getUserName();
                  this.messageTranscript.push(data);
                  this.msg.push(data)
                  this.uC.scrollToBottomOfDiv("chatFrame");
            });

      }

      save(model: any, isValid: boolean) {
            this.submitted = true;
            //let _chatMessage: chatMessage = new chatMessage();
            let _chatMessage = model.message;
            if (_chatMessage != '') {
                  this.cHS.sendNewMessage(model.message) ? model.message = '' : model.message = '';
            }
            this.myForm.reset();
      }

}


