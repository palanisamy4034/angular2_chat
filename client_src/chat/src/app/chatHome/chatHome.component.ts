
import { ViewChild, Component, Input, OnInit, trigger, state, style, animate, transition, ViewContainerRef } from '../../../../node_modules/@angular/core';
import { Router, ActivatedRoute, Params } from '../../../../node_modules/@angular/router';
import { ChatMessage } from './chatHome.model';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { connect } from 'net';
import { CustHomeService } from './chatHome.service';
import * as globalVar from '../util/global';
import { UtilComponent } from '../util/util.component';
import { UtilService } from '../util/util.service';
import { UserDetail, ConvDetail } from './../util/util.model';
import {CategoryPipe} from './../util/util.component.pipe'
//import 'sockjs-client';
@Component({
	selector: 'chatHome',
	templateUrl: './chatHome.component.html',
	styleUrls: ['./chatHome.component.css'],
})

export class ChatHomeComponent implements OnInit {
	public myForm: FormGroup;
	public submitted: boolean;
	public messageTranscript: ChatMessage[] = [];
	public userDetail: UserDetail[] = [];
	public msg: any = [];
	public uId: any;
	public userData: any;
	public senderEmail:string;

	constructor(private _fb: FormBuilder, private cHS: CustHomeService, private uC: UtilComponent, private utilServ: UtilService, private rtr: Router) { }
	ngOnInit(): void {
		let _uid = sessionStorage.getItem('uid')
		globalVar.getSenderEmail() == '' ? globalVar.setSenderEmail(sessionStorage.getItem('email')) : '';
		_uid != '' ? globalVar.setSenderId(_uid) : this.rtr.navigate(['/login']);
		this.utilServ.getUserDetail(globalVar.getSenderEmail()).subscribe(data => {
			this.cHS.connectWebSocket();
		});
		this.utilServ.getUserList().subscribe(data => {
			this.userDetail.push(data);
			this.userData = this.userDetail[0];
			this.uId = globalVar.getSenderId();
		});

		this.myForm = this._fb.group({
			message: ['', <any>Validators.required]
		});
		this.cHS.onNewMessage().subscribe(data => {
			data.dtl;
			this.messageTranscript.push(data.dtl[0]);
			this.msg.push(data.dtl[0])
			this.uC.scrollToBottomOfDiv("messages");
		});

	}
	displayUserInfo(userInfo: any) {
		if (userInfo.id) {
			let _convDetail = new ConvDetail();
			_convDetail.senderId = parseInt(globalVar.getSenderId());
			_convDetail.reciverId = userInfo.id;
			globalVar.setReciverId(userInfo.id);
			this.senderEmail=userInfo.email;
			this.utilServ.getConvDetail(_convDetail).subscribe(data => {
				globalVar.setConvId(data.id);
			});

		}

	}
	save(model: any, isValid: boolean) {
		this.submitted = true;
		let _chatMessage = new ChatMessage();
		//let _chatMessage: chatMessage = new chatMessage();
		let _Message = model.message;
		if (_Message != '') {
			_chatMessage.msgContent = _Message;
			_chatMessage.msgType = 'CHAT';
			_chatMessage.chatId = globalVar.getConvId();
			_chatMessage.senderId = globalVar.getSenderId();
			this.cHS.sendNewMessage(_chatMessage) ? model.message = '' : model.message = '';
			this.msg.push(_chatMessage);
			this.myForm.reset();
			this.uC.scrollToBottomOfDiv("messages");
		}
	}

}


